import { Product } from '../product.model'

export interface Provider {
  id: number
  name: string
  city: string
  neiborhood: string
  telephone: string
  products: Product[]
}
