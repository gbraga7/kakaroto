import { Provider } from './providers/provider.model'

export interface Product {
  id: number
  name: string
  description: string
  providers: Provider[]
}
